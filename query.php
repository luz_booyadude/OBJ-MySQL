<?php
    include("bin/OBJ_mysql.php");

    //database configuration
    $config = array(
        'hostname' => 'localhost',	
        'username' => 'root',
        'password' => '',
        'database' => base64_decode($_GET['database']),
    );
	
	$command = base64_decode($_GET['query']);
	
    //creating a new MySQL Connection
    $db = new OBJ_mysql($config);
	
	$query = $db->query($command);
    $result = $query->fetchAll();
	
    //print_r($result);
	
	$json_string = json_encode($result, JSON_PRETTY_PRINT);
	
	print_r($json_string);
?>
<?php
    include("bin/OBJ_mysql.php");

    //database configuration
    $config = array(
        'hostname' => 'localhost',	
        'username' => 'root',
        'password' => '',
        'database' => $_POST['database'],
    );
	
	$row_name = explode(",", $_POST['row']);
	$value = explode(",", $_POST['values']);
	
	//check data consistency
	if(count($row_name) != count($value)){
		//_displayError("Row numbers and values are not same.");
		print_r("Row numbers and values are not same.");
		exit;
	}
	
	$combined = array_combine($row_name, $value);
	print_r($combined);
	
    //creating a new MySQL Connection
    $db = new OBJ_mysql($config);
	
    $new_data_insert = $db->insert($_POST['table'], $combined);
	if($new_data_insert){
		echo "New data inserted";
	}
	else{
		echo "An error occured during insert.";
	}
?>